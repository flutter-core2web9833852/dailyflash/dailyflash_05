import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State createState() => _MainApp();
}

class _MainApp extends State {
  int count = 1;

  void nextQuestion() {
    setState(() {
      count++;
    });
  }

  Scaffold question() {
    if (count == 1) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Profile information"),
        ),
        body: Center(
            child: Column(
          children: [
            Image.network(
              "https://images.immediate.co.uk/production/volatile/sites/3/2023/03/goku-dragon-ball-guru-824x490-11b2006-e1697471244240.jpg?quality=90&resize=556,370",
              height: 150,
            ),
            const SizedBox(
              height: 30,
            ),
            Text(
              "Khushal",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              "9090909090",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
          ],
        )),
      );
    } else if (count == 2) {
      return Scaffold(
        body: Center(
          child: Column(
            children: [
              Container(
                height: 100,
                width: 100,
                child: Image.network(
                    "https://lh3.googleusercontent.com/BuwTy9EXzV0q_JZsHcEwSVcyR4cofoSEnJhQqrl7k5_srBisrNmnxgltobu0oc5Qv6mEDInOJLts=w1440-ns-nd-rj"),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                height: 100,
                width: 100,
                child: Image.network(
                    "https://lh3.googleusercontent.com/BuwTy9EXzV0q_JZsHcEwSVcyR4cofoSEnJhQqrl7k5_srBisrNmnxgltobu0oc5Qv6mEDInOJLts=w1440-ns-nd-rj"),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                height: 100,
                width: 100,
                child: Image.network(
                    "https://lh3.googleusercontent.com/BuwTy9EXzV0q_JZsHcEwSVcyR4cofoSEnJhQqrl7k5_srBisrNmnxgltobu0oc5Qv6mEDInOJLts=w1440-ns-nd-rj"),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      );
    } else if (count == 3) {
      return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                "https://images.immediate.co.uk/production/volatile/sites/3/2023/03/artworks-000140162979-8n10yf-t500x500-1b72fdc.jpg?quality=90&fit=500,333",
                height: 200,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20)),
                    border: Border.all(color: Colors.amber, width: 1)),
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Text("Goku"),
                ),
              )
            ],
          ),
        ),
      );
    } else if (count == 4) {
      return Scaffold(
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: 100,
                width: 100,
                color: Colors.amber,
              ),
              // const SizedBox(
              //   width: 20,
              // ),
              Container(
                height: 100,
                width: 100,
                color: Colors.purple,
              ),
              // const SizedBox(
              //   width: 20,
              // ),
              Container(
                height: 100,
                width: 100,
                color: Colors.lightBlueAccent,
              ),
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Center(
          child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROe_9MUXyH0LETLkGCFLwiS582moolGQgTZXLRCwU-BzmSWWA8SKug6vmThsLEOfMfIFc&usqp=CAU",
                  height: 100,
                  width: 100,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      height: 100,
                      width: 100,
                      color: Colors.red,
                    ),
                    Container(
                      height: 100,
                      width: 100,
                      color: Colors.blue,
                    ),
                  ],
                )
              ]),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: question(),
      floatingActionButton: FloatingActionButton(
        onPressed: nextQuestion,
        child: const Text("Next"),
      ),
    ));
  }
}
